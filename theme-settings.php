<?php

function polpo_settings($saved_settings) {
	$settings = theme_get_settings('polpo');
	$defaults = array(
		'title_text' => 1,
		'title_text_custom' => '',
	);
	$settings = array_merge($defaults, $settings);

  $form['title_text'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable Title Text'),
		'#description' => t('Uncheck the box above to disable the site name in the title bar'),
		'#default_value' => $settings['title_text'],
	);
  $form['title_text_custom'] = array(
		'#type' => 'textfield',
		'#title' => t('Custom Title Text'),
		'#description' => t('Enter custom text to appear in the title bar instead of the site name'),
		'#default_value' => $settings['title_text_custom'],
	);
  return $form;
}
