<?php

/**
 * Implements template_preprocess_page().
 */
function polpo_preprocess_page(&$vars) {
  // Add title text.
	if (theme_get_setting('title_text')) {
		if (theme_get_setting('title_text_custom')) {
			$vars['title_text'] = theme_get_setting('title_text_custom');
		} else {
			$vars['title_text'] = variable_get('site_name', 'drupal');
		}
	}

	// Hook into color.module
	if (module_exists('color')) {
		_color_page_alter($vars);
	}
}
